# By: Michael Hemmett

# Requirements
import uproot
import numpy as np
from numpy import savetxt, loadtxt
import pandas as pd
import matplotlib.pyplot as plt
import math

# Import signal and background as pandas
sig = pd.read_csv('tt-chi-sum.csv')
bkg = pd.read_csv('bbjj-chi-sum.csv')


# Plot every variable for signal and background against each other
for variable in sig:
    plt.hist(sig[variable], bins=100, linewidth=2, histtype='step', color='red', label='TT Signal')
    plt.hist(bkg[variable], bins=100, linewidth=2, histtype='step', color='blue', label='QCD Background')
    plt.legend()
    plt.grid()
    #plt.xlabel('[GeV]')
    #plt.xlim(-100, 3000)
    plt.ylabel('Entries/Bin [N]')
    plt.title(variable)
    plt.show()



