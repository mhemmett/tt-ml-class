
# Requirements
import uproot
import numpy as np
from numpy import loadtxt, savetxt
import pandas as pd
import matplotlib.pyplot as plt
import math


# Import signal and background
bkg = uproot.open('jjbb.root')
sig = uproot.open('tt_new.root')


bkg_tree1 = bkg['b4events;12']

sig_tree1 = sig['b3events;60']

sig_tree2 = sig['b4events;61']


bkg_arr = bkg_tree1.arrays(bkg_tree1.keys())

bkg_keys = bkg_tree1.keys()

bkg_arr = np.array(bkg_arr)

bkg_pd = pd.DataFrame(bkg_arr)



sig_arr = sig_tree2.arrays(sig_tree2.keys())

sig_keys = sig_tree1.keys()

sig_arr = np.array(sig_arr)

sig_pd = pd.DataFrame(sig_arr)


# Smaller sample of data
import random

sig_pd = sig_pd.sample(3000000)
bkg_pd = bkg_pd.sample(3000000)


# Plot jet variables
plt.hist(sig_pd['j1pT'], bins= 200, linewidth=2, color='red', label='j1pT', histtype='step')
plt.hist(sig_pd['j2pT'], bins= 200, linewidth=2, color='blue', label='j2pT', histtype='step')
plt.hist(sig_pd['j3pT'], bins= 200, linewidth=2, color='green', label='j3pT', histtype='step')
plt.hist(sig_pd['j4pT'], bins= 200, linewidth=2, color='orange', label='j4pT', histtype='step')
plt.hist(sig_pd['j5pT'], bins= 200, linewidth=2, color='purple', label='j5pT', histtype='step')
plt.hist(sig_pd['j6pT'], bins= 200, linewidth=2, color='black', label='j6pT', histtype='step')
plt.xlim(0,400)
plt.title('tt Signal Jets')
plt.legend()
plt.grid()
plt.xlabel('pT [GeV]')
plt.ylabel('Entries/Bin')
plt.show()

plt.hist(bkg_pd['j1pT'], bins= 200, linewidth=2, color='red', label='j1pT', histtype='step')
plt.hist(bkg_pd['j2pT'], bins= 200, linewidth=2, color='blue', label='j2pT', histtype='step')
plt.hist(bkg_pd['j3pT'], bins= 200, linewidth=2, color='green', label='j3pT', histtype='step')
plt.hist(bkg_pd['j4pT'], bins= 200, linewidth=2, color='orange', label='j4pT', histtype='step')
plt.hist(bkg_pd['j5pT'], bins= 200, linewidth=2, color='purple', label='j5pT', histtype='step')
plt.hist(bkg_pd['j6pT'], bins= 200, linewidth=2, color='black', label='j6pT', histtype='step')
plt.xlim(0,400)
plt.title('bbjj Background Jets')
plt.legend()
plt.grid()
plt.xlabel('pT [GeV]')
plt.ylabel('Entries/Bin')
plt.show()


# Construct jet variables:

# Px = pT * cos(Phi)
# Py = pT * sin(Phi)
# Pz = pT * sinh(Eta)
# E = sqrt(Px^2 + Py^2 + Pz^2)
# P1 dot P2= sqrt(Px1*Px2 + Py1*Py2 + Pz1*Pz2), etc.
# Momentum dot = Sum over nm (Pn dot Pm)
# Energy scalar = (Sum over n (En))^2
# Mass = sqrt(Energy scalar - Momentum dot)


# Variables for Jet 1

sig_pd['j1Px'] = sig_pd['j1pT'] * sig_pd['j1Phi'].apply(lambda x: math.cos(x))
sig_pd['j1Py'] = sig_pd['j1pT'] * sig_pd['j1Phi'].apply(lambda x: math.sin(x))
sig_pd['j1Pz'] = sig_pd['j1pT'] * sig_pd['j1Eta'].apply(lambda x: math.sinh(x))

sig_pd['j1Px2'] = sig_pd['j1Px'].apply(lambda x: x ** 2)
sig_pd['j1Py2'] = sig_pd['j1Py'].apply(lambda x: x ** 2)
sig_pd['j1Pz2'] = sig_pd['j1Pz'].apply(lambda x: x ** 2)

sig_pd['j1E2'] = sig_pd['j1Px2'] + sig_pd['j1Py2'] + sig_pd['j1Pz2']
sig_pd['j1E'] = sig_pd['j1E2'].apply(lambda x: math.sqrt(x))


# Variables for Jet 2

sig_pd['j2Px'] = sig_pd['j2pT'] * sig_pd['j2Phi'].apply(lambda x: math.cos(x))
sig_pd['j2Py'] = sig_pd['j2pT'] * sig_pd['j2Phi'].apply(lambda x: math.sin(x))
sig_pd['j2Pz'] = sig_pd['j2pT'] * sig_pd['j2Eta'].apply(lambda x: math.sinh(x))

sig_pd['j2Px2'] = sig_pd['j2Px'].apply(lambda x: x ** 2)
sig_pd['j2Py2'] = sig_pd['j2Py'].apply(lambda x: x ** 2)
sig_pd['j2Pz2'] = sig_pd['j2Pz'].apply(lambda x: x ** 2)

sig_pd['j2E2'] = sig_pd['j2Px2'] + sig_pd['j2Py2'] + sig_pd['j2Pz2']
sig_pd['j2E'] = sig_pd['j2E2'].apply(lambda x: math.sqrt(x))


# Variables for Jet 3

sig_pd['j3Px'] = sig_pd['j3pT'] * sig_pd['j3Phi'].apply(lambda x: math.cos(x))
sig_pd['j3Py'] = sig_pd['j3pT'] * sig_pd['j3Phi'].apply(lambda x: math.sin(x))
sig_pd['j3Pz'] = sig_pd['j3pT'] * sig_pd['j3Eta'].apply(lambda x: math.sinh(x))

sig_pd['j3Px2'] = sig_pd['j3Px'].apply(lambda x: x ** 2)
sig_pd['j3Py2'] = sig_pd['j3Py'].apply(lambda x: x ** 2)
sig_pd['j3Pz2'] = sig_pd['j3Pz'].apply(lambda x: x ** 2)

sig_pd['j3E2'] = sig_pd['j3Px2'] + sig_pd['j3Py2'] + sig_pd['j3Pz2']
sig_pd['j3E'] = sig_pd['j3E2'].apply(lambda x: math.sqrt(x))


# Variables for Jet 4

sig_pd['j4Px'] = sig_pd['j4pT'] * sig_pd['j4Phi'].apply(lambda x: math.cos(x))
sig_pd['j4Py'] = sig_pd['j4pT'] * sig_pd['j4Phi'].apply(lambda x: math.sin(x))
sig_pd['j4Pz'] = sig_pd['j4pT'] * sig_pd['j4Eta'].apply(lambda x: math.sinh(x))

sig_pd['j4Px2'] = sig_pd['j4Px'].apply(lambda x: x ** 2)
sig_pd['j4Py2'] = sig_pd['j4Py'].apply(lambda x: x ** 2)
sig_pd['j4Pz2'] = sig_pd['j4Pz'].apply(lambda x: x ** 2)

sig_pd['j4E2'] = sig_pd['j4Px2'] + sig_pd['j4Py2'] + sig_pd['j4Pz2']
sig_pd['j4E'] = sig_pd['j4E2'].apply(lambda x: math.sqrt(x))


# Variables for Jet 5

sig_pd['j5Px'] = sig_pd['j5pT'] * sig_pd['j5Phi'].apply(lambda x: math.cos(x))
sig_pd['j5Py'] = sig_pd['j5pT'] * sig_pd['j5Phi'].apply(lambda x: math.sin(x))
sig_pd['j5Pz'] = sig_pd['j5pT'] * sig_pd['j5Eta'].apply(lambda x: math.sinh(x))

sig_pd['j5Px2'] = sig_pd['j5Px'].apply(lambda x: x ** 2)
sig_pd['j5Py2'] = sig_pd['j5Py'].apply(lambda x: x ** 2)
sig_pd['j5Pz2'] = sig_pd['j5Pz'].apply(lambda x: x ** 2)

sig_pd['j5E2'] = sig_pd['j5Px2'] + sig_pd['j5Py2'] + sig_pd['j5Pz2']
sig_pd['j5E'] = sig_pd['j5E2'].apply(lambda x: math.sqrt(x))


# Variables for Jet 6

sig_pd['j6Px'] = sig_pd['j6pT'] * sig_pd['j6Phi'].apply(lambda x: math.cos(x))
sig_pd['j6Py'] = sig_pd['j6pT'] * sig_pd['j6Phi'].apply(lambda x: math.sin(x))
sig_pd['j6Pz'] = sig_pd['j6pT'] * sig_pd['j6Eta'].apply(lambda x: math.sinh(x))

sig_pd['j6Px2'] = sig_pd['j6Px'].apply(lambda x: x ** 2)
sig_pd['j6Py2'] = sig_pd['j6Py'].apply(lambda x: x ** 2)
sig_pd['j6Pz2'] = sig_pd['j6Pz'].apply(lambda x: x ** 2)

sig_pd['j6E2'] = sig_pd['j6Px2'] + sig_pd['j4Py2'] + sig_pd['j4Pz2']
sig_pd['j6E'] = sig_pd['j6E2'].apply(lambda x: math.sqrt(x))


# Momentum
sig_pd['j1P-dot-j1P'] = sig_pd['j1E2']
sig_pd['j1P-dot-j2P'] = (sig_pd['j1Px'] * sig_pd['j2Px']) + (sig_pd['j1Py'] * sig_pd['j2Py']) + (sig_pd['j1Pz'] * sig_pd['j2Pz'])
sig_pd['j1P-dot-j3P'] = (sig_pd['j1Px'] * sig_pd['j3Px']) + (sig_pd['j1Py'] * sig_pd['j3Py']) + (sig_pd['j1Pz'] * sig_pd['j3Pz'])
sig_pd['j1P-dot-j4P'] = (sig_pd['j1Px'] * sig_pd['j4Px']) + (sig_pd['j1Py'] * sig_pd['j4Py']) + (sig_pd['j1Pz'] * sig_pd['j4Pz'])
sig_pd['j1P-dot-j5P'] = (sig_pd['j1Px'] * sig_pd['j5Px']) + (sig_pd['j1Py'] * sig_pd['j5Py']) + (sig_pd['j1Pz'] * sig_pd['j5Pz'])
sig_pd['j1P-dot-j6P'] = (sig_pd['j1Px'] * sig_pd['j6Px']) + (sig_pd['j1Py'] * sig_pd['j6Py']) + (sig_pd['j1Pz'] * sig_pd['j6Pz'])

sig_pd['j2P-dot-j2P'] = sig_pd['j2E2']
sig_pd['j2P-dot-j3P'] = (sig_pd['j2Px'] * sig_pd['j3Px']) + (sig_pd['j2Py'] * sig_pd['j3Py']) + (sig_pd['j2Pz'] * sig_pd['j3Pz'])
sig_pd['j2P-dot-j4P'] = (sig_pd['j2Px'] * sig_pd['j4Px']) + (sig_pd['j2Py'] * sig_pd['j4Py']) + (sig_pd['j2Pz'] * sig_pd['j4Pz'])
sig_pd['j2P-dot-j5P'] = (sig_pd['j2Px'] * sig_pd['j5Px']) + (sig_pd['j2Py'] * sig_pd['j5Py']) + (sig_pd['j2Pz'] * sig_pd['j5Pz'])
sig_pd['j2P-dot-j6P'] = (sig_pd['j2Px'] * sig_pd['j6Px']) + (sig_pd['j2Py'] * sig_pd['j6Py']) + (sig_pd['j2Pz'] * sig_pd['j6Pz'])

sig_pd['j3P-dot-j3P'] = sig_pd['j3E2']
sig_pd['j3P-dot-j4P'] = (sig_pd['j3Px'] * sig_pd['j4Px']) + (sig_pd['j3Py'] * sig_pd['j4Py']) + (sig_pd['j3Pz'] * sig_pd['j4Pz'])
sig_pd['j3P-dot-j5P'] = (sig_pd['j3Px'] * sig_pd['j5Px']) + (sig_pd['j3Py'] * sig_pd['j5Py']) + (sig_pd['j3Pz'] * sig_pd['j5Pz'])
sig_pd['j3P-dot-j6P'] = (sig_pd['j3Px'] * sig_pd['j6Px']) + (sig_pd['j3Py'] * sig_pd['j6Py']) + (sig_pd['j3Pz'] * sig_pd['j6Pz'])

sig_pd['j4P-dot-j4P'] = sig_pd['j4E2']
sig_pd['j4P-dot-j5P'] = (sig_pd['j4Px'] * sig_pd['j5Px']) + (sig_pd['j4Py'] * sig_pd['j5Py']) + (sig_pd['j4Pz'] * sig_pd['j5Pz'])
sig_pd['j4P-dot-j6P'] = (sig_pd['j4Px'] * sig_pd['j6Px']) + (sig_pd['j4Py'] * sig_pd['j6Py']) + (sig_pd['j4Pz'] * sig_pd['j6Pz'])

sig_pd['j5P-dot-j5P'] = sig_pd['j5E2']
sig_pd['j5P-dot-j6P'] = (sig_pd['j5Px'] * sig_pd['j6Px']) + (sig_pd['j5Py'] * sig_pd['j6Py']) + (sig_pd['j5Pz'] * sig_pd['j6Pz'])

sig_pd['j6P-dot-j6P'] = sig_pd['j6E2']


# Construct jet masses: 

#1-2, 1-3, 1-4, 1-5, 1-6

# 1-2 Mass:
sig_pd['j12E-sum'] = sig_pd['j1E'] + sig_pd['j2E']
sig_pd['j12E-scalar'] = sig_pd['j12E-sum'].apply(lambda x: x ** 2)

sig_pd['j12P-dot'] = (sig_pd['j1P-dot-j1P'] + sig_pd['j1P-dot-j2P'] + sig_pd['j2P-dot-j2P'])

sig_pd['j12M2'] = sig_pd['j12E-scalar'] - sig_pd['j12P-dot']
sig_pd['j12M'] = sig_pd['j12M2'].apply(lambda x: math.sqrt(x))

# 1-3 Mass:
sig_pd['j13E-sum'] = sig_pd['j1E'] + sig_pd['j3E']
sig_pd['j13E-scalar'] = sig_pd['j13E-sum'].apply(lambda x: x ** 2)

sig_pd['j13P-dot'] = (sig_pd['j1P-dot-j1P'] + sig_pd['j1P-dot-j3P'] + sig_pd['j3P-dot-j3P'])

sig_pd['j13M2'] = sig_pd['j13E-scalar'] - sig_pd['j13P-dot']
sig_pd['j13M'] = sig_pd['j13M2'].apply(lambda x: math.sqrt(x))

# 1-4 Mass:
sig_pd['j14E-sum'] = sig_pd['j1E'] + sig_pd['j4E']
sig_pd['j14E-scalar'] = sig_pd['j14E-sum'].apply(lambda x: x ** 2)

sig_pd['j14P-dot'] = (sig_pd['j1P-dot-j1P'] + sig_pd['j1P-dot-j4P'] + sig_pd['j4P-dot-j4P'])

sig_pd['j14M2'] = sig_pd['j14E-scalar'] - sig_pd['j14P-dot']
sig_pd['j14M'] = sig_pd['j14M2'].apply(lambda x: math.sqrt(x))

# 1-5 Mass:
sig_pd['j15E-sum'] = sig_pd['j1E'] + sig_pd['j5E']
sig_pd['j15E-scalar'] = sig_pd['j15E-sum'].apply(lambda x: x ** 2)

sig_pd['j15P-dot'] = (sig_pd['j1P-dot-j1P'] + sig_pd['j1P-dot-j5P'] + sig_pd['j5P-dot-j5P'])

sig_pd['j15M2'] = sig_pd['j15E-scalar'] - sig_pd['j15P-dot']
sig_pd['j15M'] = sig_pd['j15M2'].apply(lambda x: math.sqrt(x))


# Construct jet masses: 

#2-3, 2-4, 2-5, 2-6

# 2-3 Mass:
sig_pd['j23E-sum'] = sig_pd['j2E'] + sig_pd['j3E']
sig_pd['j23E-scalar'] = sig_pd['j23E-sum'].apply(lambda x: x ** 2)

sig_pd['j23P-dot'] = (sig_pd['j2P-dot-j2P'] + sig_pd['j2P-dot-j3P'] + sig_pd['j3P-dot-j3P'])

sig_pd['j23M2'] = sig_pd['j23E-scalar'] - sig_pd['j23P-dot']
sig_pd['j23M'] = sig_pd['j23M2'].apply(lambda x: math.sqrt(x))

# 2-4 Mass:
sig_pd['j24E-sum'] = sig_pd['j2E'] + sig_pd['j4E']
sig_pd['j24E-scalar'] = sig_pd['j24E-sum'].apply(lambda x: x ** 2)

sig_pd['j24P-dot'] = (sig_pd['j2P-dot-j2P'] + sig_pd['j2P-dot-j4P'] + sig_pd['j4P-dot-j4P'])

sig_pd['j24M2'] = sig_pd['j24E-scalar'] - sig_pd['j24P-dot']
sig_pd['j24M'] = sig_pd['j24M2'].apply(lambda x: math.sqrt(x))

# 2-5 Mass:
sig_pd['j25E-sum'] = sig_pd['j2E'] + sig_pd['j5E']
sig_pd['j25E-scalar'] = sig_pd['j25E-sum'].apply(lambda x: x ** 2)

sig_pd['j25P-dot'] = (sig_pd['j2P-dot-j2P'] + sig_pd['j2P-dot-j5P'] + sig_pd['j5P-dot-j5P'])

sig_pd['j25M2'] = sig_pd['j25E-scalar'] - sig_pd['j25P-dot']
sig_pd['j25M'] = sig_pd['j25M2'].apply(lambda x: math.sqrt(x))



# Construct jet masses: 

#3-4, 3-5, 3-6, 4-5, 4-6, 5-6

# 3-4 Mass:
sig_pd['j34E-sum'] = sig_pd['j3E'] + sig_pd['j4E']
sig_pd['j34E-scalar'] = sig_pd['j34E-sum'].apply(lambda x: x ** 2)

sig_pd['j34P-dot'] = (sig_pd['j3P-dot-j3P'] + sig_pd['j3P-dot-j4P'] + sig_pd['j4P-dot-j4P'])

sig_pd['j34M2'] = sig_pd['j34E-scalar'] - sig_pd['j34P-dot']
sig_pd['j34M'] = sig_pd['j34M2'].apply(lambda x: math.sqrt(x))

# 3-5 Mass:
sig_pd['j35E-sum'] = sig_pd['j3E'] + sig_pd['j5E']
sig_pd['j35E-scalar'] = sig_pd['j35E-sum'].apply(lambda x: x ** 2)

sig_pd['j35P-dot'] = (sig_pd['j3P-dot-j3P'] + sig_pd['j3P-dot-j5P'] + sig_pd['j5P-dot-j5P'])

sig_pd['j35M2'] = sig_pd['j35E-scalar'] - sig_pd['j35P-dot']
sig_pd['j35M'] = sig_pd['j35M2'].apply(lambda x: math.sqrt(x))

# 3-6 Mass:
sig_pd['j36E-sum'] = sig_pd['j3E'] + sig_pd['j6E']
sig_pd['j36E-scalar'] = sig_pd['j36E-sum'].apply(lambda x: x ** 2)

sig_pd['j36P-dot'] = (sig_pd['j3P-dot-j3P'] + sig_pd['j3P-dot-j6P'] + sig_pd['j6P-dot-j6P'])

sig_pd['j34M2'] = sig_pd['j34E-scalar'] - sig_pd['j34P-dot']
sig_pd['j34M'] = sig_pd['j34M2'].apply(lambda x: math.sqrt(x))

# 4-5 Mass:
sig_pd['j45E-sum'] = sig_pd['j4E'] + sig_pd['j5E']
sig_pd['j45E-scalar'] = sig_pd['j45E-sum'].apply(lambda x: x ** 2)

sig_pd['j45P-dot'] = (sig_pd['j4P-dot-j4P'] + sig_pd['j4P-dot-j5P'] + sig_pd['j5P-dot-j5P'])

sig_pd['j45M2'] = sig_pd['j45E-scalar'] - sig_pd['j45P-dot']
sig_pd['j45M'] = sig_pd['j45M2'].apply(lambda x: math.sqrt(x))


# Keep only significant variables

sig_pd = sig_pd[['j1pT', 'j1Eta', 'j1Phi', 'j1Flavor',
                'j2pT', 'j2Eta', 'j2Phi', 'j2Flavor',
                'j3pT', 'j3Eta', 'j3Phi', 'j3Flavor',
                'j4pT', 'j4Eta', 'j4Phi', 'j4Flavor',
                'j5pT', 'j5Eta', 'j5Phi', 'j5Flavor',
                'j6pT', 'j6Eta', 'j6Phi', 'j6Flavor',
                'j12M', 'j13M', 'j14M','j25M',
                'j23M', 'j24M','j25M',
                'j34M','j35M','j45M']].copy()


sig_pd.to_csv('tt-jets.csv')

# Mass reconstruction repeated for bkg_pd variables


# Save smaller sample of data for efficient model training and testing
bkg_pd_from_csv = pd.read_csv('bbjj-jets.csv')
bkg_pd = bkg_pd_from_csv.sample(500000)
bkg_pd.to_csv('bbjj.csv')


sig_pd_from_csv = pd.read_csv('tt-jets.csv')
sig_pd = sig_pd_from_csv.sample(500000)
sig_pd.to_csv('tt.csv')


# Plot reconstructed masses for comparison

plt.hist(sig_pd['j12M'], bins= 500, linewidth=2, color='red', label='J1-2 Mass', histtype='step')
plt.hist(sig_pd['j34M'], bins= 500, linewidth=2, color='blue', label='J3-6 Mass', histtype='step')
plt.xlim(0,500)
plt.legend()
plt.grid()
plt.xlabel('Mass [GeV]')
plt.ylabel('Entries/Bin')
plt.show()


plt.hist(sig_pd['j12M'], bins= 500, linewidth=2, color='red', label='J1-2 Mass', histtype='step')
plt.hist(sig_pd['j35M'], bins= 500, linewidth=2, color='blue', label='J3-5 Mass', histtype='step')
plt.xlim(0,500)
plt.legend()
plt.grid()
plt.xlabel('Mass [GeV]')
plt.ylabel('Entries/Bin')
plt.show()


plt.hist(sig_pd['j12M'], bins= 500, linewidth=2, color='red', label='J1-2 Mass', histtype='step')
plt.hist(sig_pd['j45M'], bins= 500, linewidth=2, color='blue', label='J4-5 Mass', histtype='step')
plt.xlim(0,500)
plt.legend()
plt.grid()
plt.xlabel('Mass [GeV]')
plt.ylabel('Entries/Bin')
plt.show()



plt.hist(sig_pd['j13M'], bins= 500, linewidth=2, color='red', label='J1-3 Mass', histtype='step')
plt.hist(sig_pd['j24M'], bins= 500, linewidth=2, color='blue', label='J2-4 Mass', histtype='step')
plt.xlim(0,500)
plt.legend()
plt.grid()
plt.xlabel('Mass [GeV]')
plt.ylabel('Entries/Bin')
plt.show()


plt.hist(sig_pd['j13M'], bins= 500, linewidth=2, color='red', label='J1-3 Mass', histtype='step')
plt.hist(sig_pd['j25M'], bins= 500, linewidth=2, color='blue', label='J2-5 Mass', histtype='step')
plt.xlim(0,500)
plt.legend()
plt.grid()
plt.xlabel('Mass [GeV]')
plt.ylabel('Entries/Bin')
plt.show()


plt.hist(sig_pd['j13M'], bins= 500, linewidth=2, color='red', label='J1-3 Mass', histtype='step')
plt.hist(sig_pd['j45M'], bins= 500, linewidth=2, color='blue', label='J2-4 Mass', histtype='step')
plt.xlim(0,500)
plt.legend()
plt.grid()
plt.xlabel('Mass [GeV]')
plt.ylabel('Entries/Bin')
plt.show()

