
import uproot
import numpy as np
from numpy import savetxt, loadtxt
import pandas as pd
import matplotlib.pyplot as plt
import math


bkg = pd.read_csv('bbjj.csv')
sig = pd.read_csv('tt.csv')



bkg = bkg.drop(axis=1, labels=['Unnamed: 0.1', 'Unnamed: 0'])
sig = sig.drop(axis=1, labels=['Unnamed: 0.1', 'Unnamed: 0'])


# W Boson chi-squared

sig['j12M-W'] = sig['j12M'].apply(lambda x: ((x-80.4) ** 2) / 400)
sig['j13M-W'] = sig['j13M'].apply(lambda x: ((x-80.4) ** 2) / 400)
sig['j14M-W'] = sig['j14M'].apply(lambda x: ((x-80.4) ** 2) / 400)
sig['j15M-W'] = sig['j25M'].apply(lambda x: ((x-80.4) ** 2) / 400)

sig['j23M-W'] = sig['j23M'].apply(lambda x: ((x-80.4) ** 2) / 400)
sig['j24M-W'] = sig['j24M'].apply(lambda x: ((x-80.4) ** 2) / 400)
sig['j25M-W'] = sig['j25M.1'].apply(lambda x: ((x-80.4) ** 2) / 400)

sig['j34M-W'] = sig['j34M'].apply(lambda x: ((x-80.4) ** 2) / 400)
sig['j35M-W'] = sig['j35M'].apply(lambda x: ((x-80.4) ** 2) / 400)

sig['j45M-W'] = sig['j45M'].apply(lambda x: ((x-80.4) ** 2) / 400)

sig['Chi-W'] = (sig['j12M-W'] + sig['j13M-W'] + sig['j14M-W'] 
                + sig['j15M-W'] + sig['j23M-W'] + sig['j24M-W']
                + sig['j25M-W'] + sig['j34M-W'] + sig['j35M-W'] + sig['j45M-W'])

# Top quark chi-squared

sig['j12M-T'] = sig['j12M'].apply(lambda x: ((x-172.76) ** 2) / 400)
sig['j13M-T'] = sig['j13M'].apply(lambda x: ((x-172.76) ** 2) / 400)
sig['j14M-T'] = sig['j14M'].apply(lambda x: ((x-172.76) ** 2) / 400)
sig['j15M-T'] = sig['j25M'].apply(lambda x: ((x-172.76) ** 2) / 400)

sig['j23M-T'] = sig['j23M'].apply(lambda x: ((x-172.76) ** 2) / 400)
sig['j24M-T'] = sig['j24M'].apply(lambda x: ((x-172.76) ** 2) / 400)
sig['j25M-T'] = sig['j25M.1'].apply(lambda x: ((x-172.76) ** 2) / 400)

sig['j34M-T'] = sig['j34M'].apply(lambda x: ((x-172.76) ** 2) / 400)
sig['j35M-T'] = sig['j35M'].apply(lambda x: ((x-172.76) ** 2) / 400)

sig['j45M-T'] = sig['j45M'].apply(lambda x: ((x-172.76) ** 2) / 400)

sig['Chi-T'] = (sig['j12M-T'] + sig['j13M-T'] + sig['j14M-T'] 
                + sig['j15M-T'] + sig['j23M-T'] + sig['j24M-T']
                + sig['j25M-T'] + sig['j34M-T'] + sig['j35M-T'] + sig['j45M-T'])


display(sig)


sig = sig.drop(axis=1, labels=['j12M-W', 'j13M-W', 'j14M-W', 'j15M-W',
                               'j23M-W', 'j24M-W', 'j25M-W', 'j34M-W',
                               'j35M-W', 'j45M-W',
                               'j12M-T', 'j13M-T', 'j14M-T', 'j15M-T',
                               'j23M-T', 'j24M-T', 'j25M-T', 'j34M-T',
                               'j35M-T', 'j45M-T'])


sig.to_csv('tt-chi-sum.csv')


# Repeat for background:

bkg.to_csv('bbjj-chi-sum.csv')


sig = pd.read_csv('tt-chi-sum.csv')
bkg = pd.read_csv('bbjj-chi-sum.csv')


# Plots of all variables:
for variable in sig:
    plt.figure()
    plt.hist(sig[variable], bins=1000, linewidth=2, histtype='step', color='red', label='TT Signal')
    plt.hist(bkg[variable], bins=1000, linewidth=2, histtype='step', color='blue', label='QCD Background')
    plt.legend()
    plt.grid()
    plt.xlim(-100, 3000)
    plt.ylabel('Entries/Bin [N]')
    plt.title(variable)
    plt.show()


# Save final data:
sig = sig.drop(axis=1, labels=['Unnamed: 0', 'j25M.1'])
bkg = bkg.drop(axis=1, labels=['Unnamed: 0', 'j25M.1'])


sig.to_csv('tt-chi-sum.csv')
bkg.to_csv('bbjj-chi-sum.csv')


