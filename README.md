# TT ML Class



## Purpose
This project for the Machine Learning in Physics course at Westmont College, Spring 2024, explores several ML methods for separating top quark pair (tt) signal from a QCD background using simulated ATLAS data.

## Variables
For the six jets contained within signal and background, the variables used in training are as follows:  

j1pT - The pT of the first jet  

j1Eta - The eta coordinate of the first jet  

j1Phi - The phi coordinate of the first jet  

j1Flavor - Includes whether or not the jet is a b-jet  

j12M - The reconstructed mass of the first and second jet 

j12-W - The chi-squared for this di-jet mass compared to the W boson mass.

j12-T - The chi-squared for this di-jet mass compared to the top quark mass.

Chi-W - The sum over all W boson chi-squares in an event.

Chi-T - The sum over all top quark chi-squares in an event.

This is repeated for all six jets, and for all possible combinations between them.

## ML Pathways
Three different ML pathways are implemented and compared, as described below.  

BDT Classifier: Built in XGBoost. 

NN Classifier:  Built in TensorFlow with Keras.

Anomaly Detection:  Autoencoder built in TensorFlow with Keras.


## Authors and acknowledgment
Authors: Becca Hudson, Michael Lew, Berit Lundstad, Michael Hemmett, Dr. Ben Carlson (Westmont College).  

Acknowledgement: Dr. Ben Carlson, for advising the project and course.
## License
For open source projects, say how it is licensed.

## Project status
This project is ongoing as of April 2024 and is unlikely to be updated beyond May 2024.
